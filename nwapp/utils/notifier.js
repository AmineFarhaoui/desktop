'use strict';

const objectAssign = require('object-assign');

const preferences = require('./user-preferences');
const SOUNDS_ITEMS = require('../components/sound-items');

function playNotificationSound() {
  let audio = window.document.createElement('audio');
  const sound = SOUNDS_ITEMS[preferences.notificationSound];

  if (!sound.path) {
    return;
  }

  audio.src = sound.path;
  audio.play();
  audio = null;
}

const notifierDefaults = {
  title: '',
  message: '',
  // gitterHQ logo
  icon: 'https://avatars.githubusercontent.com/u/5990364?s=60',
  click: undefined,
};

module.exports = function (options) {
  const opts = objectAssign({}, notifierDefaults, options);

  if (!preferences.showNotifications) return;

  playNotificationSound();

  const notification = new window.Notification(opts.title, {
    body: opts.message,
    icon: opts.icon,
  });

  notification.onclick = () => {
    if (opts.click) {
      opts.click();
    }
  };
};
